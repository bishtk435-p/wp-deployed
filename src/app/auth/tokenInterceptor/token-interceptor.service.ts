import { Injectable, Injector } from '@angular/core';
import {
  HttpHandler,
  HttpInterceptor,
  HttpRequest,
} from '@angular/common/http';
import { AuthService } from '../auth.service';
import { MessageService } from 'src/app/core/Services/message.service';
import { Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root',
})
export class TokenInterceptorService implements HttpInterceptor {
  constructor(
    private injector: Injector,
    private message: MessageService,
    private router: Router,
    private authService: AuthService
  ) {}

  intercept(req: HttpRequest<any>, next: HttpHandler): any {
    const authService = this.injector.get(AuthService);
    req = req.clone({
      setHeaders: {
        Authorization: `Bearer ${authService.getToken()}`,
      },
    });
    return next.handle(req).pipe(
      catchError((error): any => {
        if (error.status === 401 || error.status === 403) {
          this.message.showMessage('error', error.error);
          localStorage.removeItem('token');
          localStorage.removeItem('userName');
          localStorage.removeItem('userid');
          localStorage.removeItem('userRole');
          this.router.navigate(['/']);
          this.authService.isuserLoggedIn.emit('logout');
        }
        return throwError(error);
      })
    );
  }
}
