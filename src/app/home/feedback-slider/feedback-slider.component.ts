import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'ws-feedback-slider',
  templateUrl: './feedback-slider.component.html',
  styleUrls: ['./feedback-slider.component.less']
})
export class FeedbackSliderComponent implements OnInit {
  array = [1, 2, 3, 4];
  constructor() { }

  ngOnInit(): void{
  }

}
