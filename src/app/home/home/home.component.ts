import { Component, OnInit } from '@angular/core';
import { Title } from '@angular/platform-browser';

@Component({
  selector: 'ws-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.less'],
})
export class HomeComponent implements OnInit {

  profileInfoDummy = [
    {
    _id: '1',
    fullName: 'Ashutosh Jaiswal',
    profilePicURL: 'https://img.etimg.com/thumb/msid-71917504,width-650,imgsize-623426,,resizemode-4,quality-100/untitled-3.jpg',
    rating: 4.3,
    jobsCompleted: 23,
    skills: ['Plumber', 'Gardener'],
    ratesForAnHour: 560,
    atWork: false
  },
  {
    _id: '2',
    fullName: 'Ajay Pathak',
    profilePicURL: 'https://images.newindianexpress.com/uploads/user/imagelibrary/2020/4/2/w900X450/Rishabh_Pant_PTI.jpg',
    rating: 4.1,
    jobsCompleted: 109,
    skills: ['Car Mechanic'],
    ratesForAnHour: 490,
    atWork: true
  },
  {
    _id: '3',
    fullName: 'Aditya Rauthan',
    profilePicURL: 'https://www.cricbuzz.com/a/img/v1/152x152/i1/c154520/ishant-sharma.jpg',
    rating: 3.1,
    jobsCompleted: 49,
    skills: ['Painter', 'Home Tutor', 'Carpenter'],
    ratesForAnHour: 899,
    atWork: true
  }
];

  constructor(private titleService: Title) {}

  ngOnInit(): void {
    this.titleService.setTitle('Welcome to Work point');
  }
}
