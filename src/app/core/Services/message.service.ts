import { Injectable } from '@angular/core';
import { NzMessageService } from 'ng-zorro-antd/message';
import { TitleCasePipe } from '@angular/common';

@Injectable({
  providedIn: 'root'
})
export class MessageService {

  constructor(
    private message: NzMessageService
  ) { }

  showMessage(type: string, showMessage: string): void {
    switch (type) {
      case 'success': {
        this.message.success(new TitleCasePipe().transform(showMessage));
        break;
      }
      case 'error': {
        this.message.error(new TitleCasePipe().transform(showMessage));
        break;
      }
      case 'warning': {
        this.message.warning(new TitleCasePipe().transform(showMessage));
        break;
      }
      case 'info': {
        this.message.info(new TitleCasePipe().transform(showMessage));
        break;
      }
      case 'loading': {
        this.message.loading(new TitleCasePipe().transform(showMessage));
        break;
      }
    }
  }
}
