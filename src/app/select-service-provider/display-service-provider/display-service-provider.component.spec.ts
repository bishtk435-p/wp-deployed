import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DisplayServiceProviderComponent } from './display-service-provider.component';

describe('DisplayServiceProviderComponent', () => {
  let component: DisplayServiceProviderComponent;
  let fixture: ComponentFixture<DisplayServiceProviderComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DisplayServiceProviderComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DisplayServiceProviderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
