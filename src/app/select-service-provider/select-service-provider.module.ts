import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DisplayServiceProviderComponent } from './display-service-provider/display-service-provider.component';
import { FiltersComponent } from './filters/filters.component';
import { SelectedServiceProviderComponent } from './selected-service-provider/selected-service-provider.component';
import { ServicesRoutingModule } from './services-routing.modules';
import { NzGridModule } from 'ng-zorro-antd/grid';
import { NzLayoutModule } from 'ng-zorro-antd/layout';
import { NzMenuModule } from 'ng-zorro-antd/menu';
import { WsCommonModule } from '../common/common.module';
import { FormsModule } from '@angular/forms';

@NgModule({
  declarations: [
    DisplayServiceProviderComponent,
    FiltersComponent,
    SelectedServiceProviderComponent,
  ],
  imports: [
    CommonModule,
    ServicesRoutingModule,
    NzGridModule,
    NzLayoutModule,
    NzMenuModule,
    WsCommonModule,
    FormsModule,
  ],
  exports: [],
})
export class SelectServiceProviderModule {}
