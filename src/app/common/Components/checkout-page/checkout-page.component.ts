import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { NzModalService } from 'ng-zorro-antd/modal';
import { AdminApiService } from 'src/app/core/Services/admin-api.service';
import { ApiService } from 'src/app/core/Services/api.service';
import { CartService } from 'src/app/core/Services/cart.service';
import { MessageService } from 'src/app/core/Services/message.service';
import { SelectServiceProviderFormComponent } from '../select-service-provider-form/select-service-provider-form.component';
import { Socket } from 'ngx-socket-io';
@Component({
  selector: 'ws-checkout-page',
  templateUrl: './checkout-page.component.html',
  styleUrls: ['./checkout-page.component.less']
})
export class CheckoutPageComponent implements OnInit {
  selectedData = []
  category: any;
  constructor(
    private cartService: CartService,
    private modalService: NzModalService,
    private apiService: ApiService,
    private adminService: AdminApiService,
    private msgService: MessageService,
    private router: Router,
    private socket: Socket
  ) { }

  ngOnInit(): void {
    this.cartService.selectedServiceProviders$.subscribe( res =>{
      this.selectedData = res;
    })
    console.log(this.selectedData)
    this.getCategory();
  }

  index = 0;

  totalStep = [0,1];

  onIndexChange(event: number): void {
    this.index = event;
  }
  next(){
    // if(this.index < 2)
    // this.index++;
    if(this.index === 0){
      let sendData = [];
      let selectedServiceProvidersId = [];
      this.selectedData.forEach(element => {
        sendData.push(element.formData);
        selectedServiceProvidersId.push(element.formData.requestedTo);
      });
    this.apiService.saveUserRequest(sendData).subscribe((res) => {
      console.log('Api respons==', res);
      this.cartService.selectedServiceProviders = []
      this.cartService.updateSelectedServiceProviders();
      localStorage.removeItem('cartItems');   
      this.msgService.showMessage('success',res.message);  
      this.router.navigate(['/seller/requested-services']); 
    },
    err => {
      this.msgService.showMessage('error', err);
    }
    );

      this.apiService.saveUserRequest(sendData).subscribe((res) => {
      this.socket.emit('Received New Service Request', {serviceProvidersId: selectedServiceProvidersId});
    });

    }
  }

  before(){
    if (this.index > 0) {
    this.index--;
    }
  }

  EditDetail(value): any{
    this.modalService.create({
      nzTitle: null,
      nzFooter: null,
      nzMaskClosable: false,
      // nzWidth: 900,
      // nzBodyStyle: {top:"0px",height: "600px"},
      nzComponentParams: {data: value , edit: true},
      nzContent: SelectServiceProviderFormComponent,
    });
  }

  getCategory(): void {
    this.adminService.getCategory().subscribe((res: any) => {
      this.category = res;
    });
  }

}
