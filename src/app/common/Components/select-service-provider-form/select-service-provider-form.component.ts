import { Component, Input, OnInit } from '@angular/core';
import { NzModalRef } from 'ng-zorro-antd/modal';
import { ApiService } from 'src/app/core/Services/api.service';
import { CartService } from 'src/app/core/Services/cart.service';
import { MessageService } from 'src/app/core/Services/message.service';

@Component({
  selector: 'ws-select-service-provider-form',
  templateUrl: './select-service-provider-form.component.html',
  styleUrls: ['./select-service-provider-form.component.less'],
})
export class SelectServiceProviderFormComponent implements OnInit {
  @Input() data: any;
  @Input() edit: any;

  date = null;
  workDiscription = 'abc';
  starttime = null;
  endtime = null;
  address = localStorage.getItem('userAddress') || '';
  constructor(
    private modal: NzModalRef,
    private cartService: CartService,
    private apiService: ApiService,
    private msgService: MessageService
  ) {}

  ngOnInit(): void {
    if(this.edit){
      this.date = this.data.workingDate
      this.workDiscription = this.data.description
      this.starttime = this.data.startTime
      this.endtime = this.data.endTime
      this.address = this.data.location
    }
    console.log(this.data);
  }

  checkform(): any {
    if (
      this.date == null ||
      this.workDiscription === '' ||
      this.starttime == null ||
      this.endtime == null ||
      this.address === ''
    ) {
      return true;
    } else {
      return false;
    }
  }

  closeDialog(): any {
    this.modal.destroy();
  }

  SaveData(): any {
    if (this.edit) {
      this.cartService.selectedServiceProviders.forEach((element) => {
        if (element.formData.requestedTo === this.data.requestedTo) {
          element.formData = {
            requestBy: localStorage.getItem('userid'),
            requestedTo: this.data.requestedTo,
            description: this.workDiscription,
            startTime: this.starttime,
            endTime: this.endtime,
            location: this.address,
            status: 'pending',
            workingDate: this.date,
            createdDate: new Date().toString(),
            createdTime: Date.now().toString(),
          };
        }
      });
      localStorage.setItem(
        'cartItems',
        JSON.stringify(this.cartService.selectedServiceProviders)
      );
      this.cartService.updateSelectedServiceProviders();
      this.closeDialog();
      this.msgService.showMessage('success', 'Request Edited');
    } else {
      console.log('save Function Call');
      localStorage.setItem('userAddress', this.address);
      const FormData = {
        requestBy: localStorage.getItem('userid'),
        requestedTo: this.data._id,
        description: this.workDiscription,
        startTime: this.starttime,
        endTime: this.endtime,
        location: this.address,
        status: 'pending',
        workingDate: this.date,
        createdDate: new Date().getTime(),
        createdTime: Date.now().toString(),
      };
      console.log(FormData);
      // this.apiService.saveUserRequest(FormData).subscribe((res) => {
      this.cartService.emitFormData({
        formData: FormData,
        serviceProvider: this.data,
      });
      this.closeDialog();
      this.msgService.showMessage('success', 'Request Saved');
      // console.log(res);
      // });
    }
  }

  onChange(result: Date): void {
    this.date = result;
  }
  startlog(value: Date): void {
    this.starttime = value;
  }
  endlog(value: Date): void {
    this.endtime = value;
  }
}
