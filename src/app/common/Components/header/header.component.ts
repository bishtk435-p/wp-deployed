import { Component, OnInit, OnChanges } from '@angular/core';
import { NzModalRef, NzModalService } from 'ng-zorro-antd/modal';
import { Subscriber } from 'rxjs';
import { AuthService } from 'src/app/auth/auth.service';
import { LoginComponent } from 'src/app/auth/login/login.component';
import { SignupComponent } from 'src/app/auth/signup/signup.component';
import { MessageService } from '../../../core/Services/message.service';
import { NzDrawerService } from 'ng-zorro-antd/drawer';
import { DrawerComponent } from '../drawer/drawer.component';
import { Router } from '@angular/router';
import { CartDrawerComponent } from '../cart-drawer/cart-drawer.component';
import { CartService } from 'src/app/core/Services/cart.service';
import { AdminApiService } from 'src/app/core/Services/admin-api.service';
import { NotificationService } from '../../../core/Services/notification.service';
import { Socket } from 'ngx-socket-io';
@Component({
  selector: 'ws-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.less'],
})
export class HeaderComponent implements OnInit {
  isloogedin: any;
  currenUserName: any;
  currentUserRole: any;
  selectedServiceProviders = [];
  notification = [];
  servicemenu: any;
  constructor(
    private modalService: NzModalService,
    private messageService: MessageService,
    private authService: AuthService,
    private drawer: NzDrawerService,
    private router: Router,
    private cartService: CartService,
    private AdminService: AdminApiService,
    private socket: Socket,
    private notificationService: NotificationService
  ) {}

  ngOnInit(): void {
    this.isUserLoggedIn();

    this.authService.isuserLoggedIn.subscribe((res) => {
      this.isUserLoggedIn();
    });

    this.cartService.selectedServiceProviders$.subscribe((res: any) => {
      this.selectedServiceProviders = res;
    });

    this.AdminService.getCategory().subscribe((res) => {
      console.log(res);
      this.servicemenu = res;
    });
  }

  signupDialog(): void {
    this.modalService.create({
      nzTitle: null,
      nzFooter: null,
      // nzWidth: 900,
      // nzBodyStyle: {top:"0px",height: "600px"},
      nzContent: SignupComponent,
    });
  }
  loginDialog(): void {
    this.modalService.create({
      nzTitle: null,
      nzFooter: null,
      nzContent: LoginComponent,
    });
  }

  isUserLoggedIn(): any {
    this.isloogedin = this.authService.isloggedIn();
    if (this.isloogedin) {
      this.currenUserName = this.authService.getCurrentUserName();
      this.currentUserRole = this.authService.getCurrentUserRole();
      this.socket.emit('Join Private Room', {userId: localStorage.getItem('userid')});
      this.notificationService.connectNotificationSocket();
      // if (this.currentUserRole === 'admin'){
      //   this.router.navigate(['/admin']);
      // }
      // else{
      //   this.router.navigate(['/']);
      // }
    } else {
      this.currenUserName = null;
      this.currentUserRole = null;
    }
  }

  openDrawer(): void {
    this.drawer.create({
      nzContent: DrawerComponent,
      nzClosable: false,
      nzWidth: 310,
      nzPlacement: 'left',
      nzHeight: '100%',
    });
  }

  open(): void {
    this.drawer.create({
      nzContent: CartDrawerComponent,
      nzClosable: false,
      nzWidth: 400,
      nzPlacement: 'right',
      nzHeight: '100%',
    });
  }
}
