import { NgModule } from '@angular/core';
import { ServiceCardComponent } from './Components/service-card/service-card.component';
import { AntDesignModule } from '../ant-design/ant-design.module';
import { FeatureCardComponent } from './Components/feature-card/feature-card.component';
import { ProfileInfoCardComponent } from './Components/profile-info-card/profile-info-card.component';
import { HeaderComponent } from './Components/header/header.component';
import { FooterComponent } from './Components/footer/footer.component';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NzDrawerModule } from 'ng-zorro-antd/drawer';
import { DrawerComponent } from './Components/drawer/drawer.component';
import { NzMenuModule } from 'ng-zorro-antd/menu';
import { ChangePasswordComponent } from './Components/change-password/change-password.component';
import { RouterModule } from '@angular/router';
import { NzInputModule } from 'ng-zorro-antd/input';
import { ErrorPageComponent } from './Components/error-page/error-page.component';
import { ForgetPasswordComponent } from './Components/forget-password/forget-password.component';
import { ResetPasswordComponent } from './Components/reset-password/reset-password.component';
import { CartDrawerComponent } from './Components/cart-drawer/cart-drawer.component';
import { CheckoutPageComponent } from './Components/checkout-page/checkout-page.component';
import { SelectServiceProviderFormComponent } from './Components/select-service-provider-form/select-service-provider-form.component';
import { FeedbackformComponent } from './Components/feedbackform/feedbackform.component';
import { CanclePaymentComponent } from './Components/checkout-page/cancle-payment/cancle-payment.component';

@NgModule({
  declarations: [
    ServiceCardComponent,
    FeatureCardComponent,
    ProfileInfoCardComponent,
    HeaderComponent,
    FooterComponent,
    DrawerComponent,
    ChangePasswordComponent,
    ErrorPageComponent,
    ForgetPasswordComponent,
    ResetPasswordComponent,
    CartDrawerComponent,
    CheckoutPageComponent,
    SelectServiceProviderFormComponent,
    FeedbackformComponent,
    CanclePaymentComponent,
  ],
  imports: [
    AntDesignModule,
    CommonModule,
    NzDrawerModule,
    NzMenuModule,
    FormsModule,
    RouterModule,
    NzInputModule,
    ReactiveFormsModule,
  ],
  exports: [
    ServiceCardComponent,
    FeatureCardComponent,
    ProfileInfoCardComponent,
    AntDesignModule,
    HeaderComponent,
    FooterComponent,
    ErrorPageComponent,
  ],
})
export class WsCommonModule {}
