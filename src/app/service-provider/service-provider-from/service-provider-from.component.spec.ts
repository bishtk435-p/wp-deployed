import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ServiceProviderFromComponent } from './service-provider-from.component';

describe('ServiceProviderFromComponent', () => {
  let component: ServiceProviderFromComponent;
  let fixture: ComponentFixture<ServiceProviderFromComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ServiceProviderFromComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ServiceProviderFromComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
