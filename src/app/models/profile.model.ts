export interface Profile {
  _id: string;
  firstName: string;
  lastName: string;
  profileurl?: string;
  rating?: number;
  jobsCompleted?: number;
  skills: string[];
  topComments?: object[];
  pincode?: number;
  workingRate?: number;
  atWork?: boolean;
  status?: string;
  statusColor?: string;
}
