import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NzMenuModule } from 'ng-zorro-antd/menu';
import { NzInputModule } from 'ng-zorro-antd/input';
import { NzIconModule } from 'ng-zorro-antd/icon';
import { NzButtonModule } from 'ng-zorro-antd/button';
import { NzCardModule } from 'ng-zorro-antd/card';
import { NzGridModule } from 'ng-zorro-antd/grid';
import { NzModalModule } from 'ng-zorro-antd/modal';
import { NzFormModule } from 'ng-zorro-antd/form';
import { NzCheckboxModule } from 'ng-zorro-antd/checkbox';
import { NzAnchorModule } from 'ng-zorro-antd/anchor';
import { NzCarouselModule } from 'ng-zorro-antd/carousel';
import { NzMessageModule } from 'ng-zorro-antd/message';
import { NzAvatarModule } from 'ng-zorro-antd/avatar';
import { NzRateModule } from 'ng-zorro-antd/rate';
import { NzSpinModule } from 'ng-zorro-antd/spin';
import { NzTreeModule } from 'ng-zorro-antd/tree';
import { NzTabsModule } from 'ng-zorro-antd/tabs';
import { NzSelectModule } from 'ng-zorro-antd/select';
import { NzRadioModule } from 'ng-zorro-antd/radio';
import { NzSliderModule } from 'ng-zorro-antd/slider';
import { NzBadgeModule } from 'ng-zorro-antd/badge';
import { NzStepsModule } from 'ng-zorro-antd/steps';
import { NzEmptyModule } from 'ng-zorro-antd/empty';
import { NzUploadModule } from 'ng-zorro-antd/upload';
import { NgxSpinnerModule } from 'ngx-spinner';
import { NzDropDownModule } from 'ng-zorro-antd/dropdown';
import { NzDatePickerModule } from 'ng-zorro-antd/date-picker';
import { NzTableModule } from 'ng-zorro-antd/table';
import { NzTimePickerModule } from 'ng-zorro-antd/time-picker';
import { NzNotificationModule } from 'ng-zorro-antd/notification';

const antModules = [
  NzMenuModule,
  NzInputModule,
  NzIconModule,
  NzButtonModule,
  NzCardModule,
  NzGridModule,
  NzModalModule,
  NzFormModule,
  NzCheckboxModule,
  NzAnchorModule,
  NzCarouselModule,
  NzMessageModule,
  NzAvatarModule,
  NzRateModule,
  NgxSpinnerModule,
  NzSpinModule,
  NzSpinModule,
  NzTreeModule,
  NzTabsModule,
  NzSelectModule,
  NzBadgeModule,
  NzStepsModule,
  NzRadioModule,
  NzSliderModule,
  NzBadgeModule,
  NzEmptyModule,
  NzUploadModule,
  NzDropDownModule,
  NzDatePickerModule,
  NzTableModule,
  NzTimePickerModule,
  NzNotificationModule,
];

@NgModule({
  declarations: [],
  imports: [CommonModule, ...antModules],
  exports: [...antModules],
})
export class AntDesignModule {}
