import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { RequestedServicesComponent } from './requested-services/requested-services.component';
import { SellerDashboardComponent } from './seller-dashboard/seller-dashboard.component';

const routes: Routes = [
  { path: '', redirectTo: 'dashboard', pathMatch: 'full' },
  {
    path: 'dashboard',
    component: SellerDashboardComponent,
  },
  {
    path: 'requested-services',
    component: RequestedServicesComponent,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class SellerPanelRoutingModule {}
